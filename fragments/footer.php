<?php
start_section();
?>
<hr class="green">
<footer>
    <p>Copyright (C) 2012 The Internet For Idiots<br></p>

    <p class="copyright">All content is licensed via the <a
            href="http://creativecommons.org/licenses/by-nc/3.0/" target="_blank">Attribution-NonCommercial 3.0
        Unported</a> license.<br>All other copyrights belong to their respected owners.</p>
</footer>
<? end_section("footer"); ?>
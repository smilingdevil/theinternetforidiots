<?php
start_section();
?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?=\framework\view\View::$PAGE_TITLE?></title>
    <meta name="description" content="Teaching people about the internet">
    <meta name="viewport" content="width=device-width">

    <link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="<?=static_url("/css/normalize.css")?>">
    <link rel="stylesheet" href="<?=static_url("/css/main.css")?>">

    <script src="../static/js/vendor/modernizr-2.6.1-respond-1.1.0.min.js"></script>
</head>
<? end_section("includes"); ?>
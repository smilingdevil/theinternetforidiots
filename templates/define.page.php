<?php
global $_DATA, $_SECTION;

$definition = \framework\models\Definition::objects()->find("word", $_DATA['word']);
if (empty($definition)) {
    \framework\error\ErrorHandler::FourOhFour();

    return;
}
$definition = $definition[0];

// Set title
if(isset($_DATA['is_home'])) {
    // homepages
    \framework\view\View::$PAGE_TITLE = "Home | TIFI";
} else {
    // definition
    \framework\view\View::$PAGE_TITLE = htmlentities($definition->getWord()) . " | TIFI";
}


fragment("includes");
fragment("header");
fragment("sidebar");
fragment("footer");

/**
 * Word
 */
start_section();
print htmlentities($definition->getWord());
end_section("word");

/**
 * Phonetic spelling
 */
start_section();
print htmlentities($definition->getPhonetic());
end_section("phonetic");

/**
 * Last modified
 */
start_section();
print date("F jS, Y \a\\t G:i A", strtotime($definition->getLastModified()));
end_section("last_modified");

/**
 * Definition
 */
start_section();
print $definition->getDefinition();
end_section("definition");

// Important!
//fragment("define_page");

/* Render page */
start_section();
?>
<article>
    <?=$_SECTION['sidebar'];?>

    <header>
        <h1><?=$_SECTION['word']?>
            <small class="phonetic"><?=$_SECTION['phonetic']?></small>
        </h1>
        <p>Last Modified: <b>
            <time><?=$_SECTION['last_modified']?></time>
        </b></p>
    </header>
    <hr class='green'>
    <section>
        <?=$_SECTION['definition']?>
    </section>
</article>
<?
end_section("page_content");

\framework\view\View::render("base");
<?php
fragment("includes");
fragment("header");
fragment("footer");

$words = \framework\models\Definition::objects()->getIterator();

/**
 * Title
 */
start_section();
print "Word List ({$words->count()})";
end_section("title");

/**
 * List
 */
start_section();
?>
<ul>
    <?
    foreach ($words as $word) {
        ?>
        <li><a href="<?=make_path("define/" . $word->word)?>"><?=htmlentities($word->word)?></a>
        </li>
        <?
    }?>
</ul>
<?
end_section("word_list");

// Important!
start_section();
?>
<article>
    <header>
        <h1 style="text-align:center;"><?=$_SECTION['title']?></h1>
    </header>
    <hr class="green">
    <section>
        <?=$_SECTION['word_list']?>
    </section>
</article>
<?
end_section("page_content");

\framework\view\View::render("base");
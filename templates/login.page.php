<?php
global $_DATA;
\framework\view\View::$PAGE_TITLE = "Login | TIFI";

fragment("includes");
fragment("header");
fragment("footer");

// Important!
start_section();
?>
<article>
    <header>
        <h1>Login</h1>
        <hr>
        <form action="/login/" method="post" class="login">
            <label for="username">Username:</label>
            <input type="text" name="username" id="username" placeholder="Username:"/>
        </form>
    </header>
</article>
<?
end_section("page_content");

\framework\view\View::render("base");
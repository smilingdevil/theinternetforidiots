<?php
global $_SECTION;
?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<?=$_SECTION['includes']?>
<body>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser
    today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to
    better
    experience this site.</p>
<![endif]-->
<?=$_SECTION['header'];?>

<div class="main-container">
    <div class="full-main wrapper definition clearfix">
        <?=$_SECTION['page_content'];?>
    </div>
</div>
<?=$_SECTION['footer'];?>

<script src="<?=static_url("/js/jquery.min.js")?>"></script>

<script src="<?=static_url("/js/main.js")?>"></script>
</body>
</html>

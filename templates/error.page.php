<?php
global $_DATA;

// Page title
\framework\view\View::$PAGE_TITLE = $_DATA['error'] . " - " . $_DATA['error_msg'];

fragment("includes");
fragment("header");
fragment("footer");

/**
 * Error
 */
start_section();
print $_DATA['error'];
end_section('error');

/**
 * Error Subtext
 */
start_section();
print $_DATA['error_msg'];
end_section("error_subtext");

/**
 * Error Description
 */
start_section();
switch ($_DATA['error']) {
    case "404":
        print "We couldn't find the page or definition you were looking for, we're sorry!";
        break;
    case "403":
        print "You shouldn't be in here!";
        break;
    case "503":
        print "We've a bit overloaded right now, try again later!";
        break;
    default:
        print "We know about as much as you do now, sorry!";
        break;
}
end_section("error_desc");
// Important!
//fragment("error_page");
/*
 * Make page
 */
start_section();
?>
<article>
    <header>
        <h1 class="error-title"><?=$_SECTION['error']?>!</h1>
        <h2 class="subtle error-sub"><?=$_SECTION['error_subtext']?></h2>
        <hr>
        <p class="error-desc"><?=$_SECTION['error_desc']?></p>
    </header>
</article>
<?
end_section("page_content");

\framework\view\View::render("base");
<?php
require implode(DIRECTORY_SEPARATOR, array(dirname(dirname(dirname(__DIR__))), "framework", "initialize.php"));

if (!isset($_GET['word']) || empty($_GET['word'])) {
    \framework\error\ErrorHandler::FourOhThree();
} else {
    return getPronunciation($_GET['word']);
}
<?php
require implode(DIRECTORY_SEPARATOR, array(dirname(dirname(__FILE__)), "framework", "initialize.php"));

if(empty($_GET['word']))
    redirect(make_path());

$word  = $_GET['word'];
$valid = \framework\models\Definition::objects()->find("word", $_GET['word']);
if (sizeof($valid) > 0) {
    \framework\view\View::render("define", ["word" => $word]);
} else {
   \framework\error\ErrorHandler::FourOhFour();
}


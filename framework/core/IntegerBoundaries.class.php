<?php
/**
 * Framework created by Evan Darwin.
 *
 * You may not redistribute this code without written permission from Evan Darwin.
 *
 * This work is licensed under a Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
 *
 * Information about this license is located here:
 * http://creativecommons.org/licenses/by-nc-nd/3.0/legalcode
 * or view the LICENSE file included for the full license.
 */

namespace framework\core;

class IntegerBoundaries {
    private $min = 0;
    private $max = 0;

    public function __construct($min = NULL, $max = NULL) {
        if (!is_null($min) && !is_numeric($min))
            throw new \InvalidArgumentException("Invalid type provided for \$min, expected null or int, got: " . gettype($min));
        if (!is_null($max) && !is_numeric($max))
            throw new \InvalidArgumentException("Invalid type provided for \$min, expected null or int, got: " . gettype($max));
        $this->min = $min;
        $this->max = $max;
    }

    private function isInfinite($arg) {
        if ($arg != "min" || $arg != "max") {
            throw new \InvalidArgumentException("Unknown variable to check: '$arg'");
        }
        if ($arg == "min")
            return ($this->min == NULL);
        else
            return ($this->max == NULL);
    }

    public function inRange($num, $inclusive = FALSE) {
        if (!is_numeric($num)) {
            throw new \InvalidArgumentException("Expected int got " . gettype($num));
        } else {
            if ($this->isInfinite("min") and $this->isInfinite("max"))
                return TRUE;
            if ($inclusive) {
                if ($this->isInfinite("min")) {
                    return ($num <= $this->max);
                } else if ($this->isInfinite("max")) {
                    return ($num >= $this->min);
                }
            } else {
                if ($this->isInfinite("min")) {
                    return ($num < $this->max);
                } else if ($this->isInfinite("max")) {
                    return ($num > $this->min);
                }
            }
        }
    }

    public function getMin() {
        return $this->min;
    }

    public function getMax() {
        return $this->max;
    }

    public function updateMin($min) {
        if (!is_null($min) && !is_numeric($min))
            throw new \InvalidArgumentException("Invalid type provided for \$min, expected null or int, got: " . gettype($min));
        $this->min = $min;
    }

    public function updateMax($max) {
        if (!is_null($max) && !is_numeric($max))
            throw new \InvalidArgumentException("Invalid type provided for \$min, expected null or int, got: " . gettype($max));
        $this->max = $max;
    }
}
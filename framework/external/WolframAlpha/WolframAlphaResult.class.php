<?php
/**
 * Framework created by Evan Darwin.
 *
 * You may not redistribute this code without written permission from Evan Darwin.
 *
 * This work is licensed under a Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
 *
 * Information about this license is located here:
 * http://creativecommons.org/licenses/by-nc-nd/3.0/legalcode
 * or view the LICENSE file included for the full license.
 */

namespace framework\external\WolframAlpha;

define('WA_RESULT_TYPE_PRONUNCIATION', "Pronunciation:WordData");
define('WA_RESULT_TYPE_DEFINITION', "Definition:WordData");

class WolframAlphaResult {
    private $pods = array();
    private $success = FALSE;
    private $error = FALSE;
    private $gen_time = 0.0;

    public function __construct($data) {
        $data = (array)$data;

        $this->success  = $data["@attributes"]['success'];
        $this->error    = $data['@attributes']['error'];
        $this->gen_time = $data['@attributes']['timing'];

        foreach ($data['pod'] as $pod_o) {
            $this->pods[] = new WolframAlphaDataPod($pod_o);
        }
    }

    /**
     * @param $title string Query result set to search by
     *
     * @return WolframAlphaDataPod|null Returns object or null if none is found
     */
    public function getByResultSet($title) {
        foreach ($this->pods as $pod) {
            if ($pod->getResultSetTitle() == $title) {
                return $pod;
            }
        }

        return NULL;
    }

    public function getLoadTime() {
        return $this->gen_time;
    }
}

class WolframAlphaDataPod {
    private $title = "";
    private $resultType = "";
    private $error = FALSE;

    private $text = "";

    public function __construct(array $pod) {
        $this->title      = $pod['@attributes']['title'];
        $this->resultType = $pod['@attributes']['id'];

        // Just get the text instead of storing the subpod
        $this->text = $pod['subpod']['plaintext'];
        if (isset($pod['subpod']['error']))
            $this->error = $pod['subpod']['error'];
    }

    /**
     * @return string Result set type
     */
    public function getResultSetTitle() {
        return $this->resultType;
    }

    /**
     * @return string Returns if an error occured loading the pod
     */
    public function isErr() {
        return $this->error;
    }

    /**
     * @return string Returns text given
     */
    public function getText() {
        return $this->text;
    }
}
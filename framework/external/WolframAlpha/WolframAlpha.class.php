<?php
/**
 * Framework created by Evan Darwin.
 *
 * You may not redistribute this code without written permission from Evan Darwin.
 *
 * This work is licensed under a Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
 *
 * Information about this license is located here:
 * http://creativecommons.org/licenses/by-nc-nd/3.0/legalcode
 * or view the LICENSE file included for the full license.
 */

namespace framework\external\WolframAlpha;

class WolframAlphaAPI {
    private $KEY = "GHJW7V-YL2RQWPXTP";

    /**
     * @param $request string Query to execute
     * @param $key     string APPID key require for the query
     *
     * @return string URL to execute the query
     */
    private function generateQueryURL($request, $key) {
        return "http://api.wolframalpha.com/v2/query?input=" . urlencode($request) . "&appid=$key";
    }

    /**
     * @param $xml string XML to parse
     *
     * @return \stdClass|array Returns the formatted XML
     * @throws \InvalidArgumentException
     */
    private function parseXML($xml, $outputJSON = FALSE) {
        if (!@simplexml_load_string($xml)) {
            throw new \InvalidArgumentException("XML supplied is not valid XML");
        } else {
            return json_decode(json_encode(@simplexml_load_string($xml)), true);
        }
    }

    /**
     * @param $request string Query to execute
     *
     * @throws \InvalidArgumentException If the request is empty
     */
    public function query($request) {
        if (empty($request)) {
            throw new \InvalidArgumentException("WA request cannot be empty");
        } else {
            $xml = file_get_contents($this->generateQueryURL($request, $this->KEY));

            return new WolframAlphaResult($this->parseXML($xml, TRUE));
        }
    }
}

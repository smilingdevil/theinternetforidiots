<?php
/**
 * Framework created by Evan Darwin.
 *
 * You may not redistribute this code without written permission from Evan Darwin.
 *
 * This work is licensed under a Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
 *
 * Information about this license is located here:
 * http://creativecommons.org/licenses/by-nc-nd/3.0/legalcode
 * or view the LICENSE file included for the full license.
 */

namespace framework\models;

define('MODEL_ARG_MAX_LENGTH', "1");
define('MODEL_ARG_MIN_LENGTH', "2");

abstract class Model {
    /**
     * @return \framework\db\DBOBjectsList Returns a list of objects for this class
     */
    public static function objects() {
        return new \framework\db\DBOBjectsList(\R::find(strtolower(get_real_class(get_called_class()))));
    }

    /**
     * Internal function to check that we can make a new object
     * from this bean.
     *
     * @param $bean \RedBean_OODBBean Bean to check
     *
     * @return bool Is valid
     */
    protected function isValidValue($bean) {
        return (!(!$bean));
    }

    // Length constrictions for new value
    protected $length_constrictions = NULL;

    // Bean object
    protected $bean = NULL;

    /**
     * @param \RedBean_OODBBean $bean Bean to create object from
     * @param array             $args Array of arguments to supply
     */
    public function __construct(\RedBean_OODBBean $bean, array $args = array()) {
        if ($this->isValidValue($bean))
            $this->bean = $bean;

        $this->length_constrictions = new \framework\core\IntegerBoundaries();

        foreach ($args as $arg => $value) {
            if (!is_numeric($arg) || !is_numeric($value)) {
                throw new \InvalidArgumentException("Invalid argument supplied: " + $arg);
            }
            if ($arg == MODEL_ARG_MAX_LENGTH) {
                $this->length_constrictions->updateMax($value);
            } else if ($arg == MODEL_ARG_MIN_LENGTH) {
                $this->length_constrictions->updateMin($value);
            }
        }
    }

    /**
     * Returns a new object from an ID
     *
     * @param $id int Return an object from it's ID
     *
     * @return Definition Definition object matching the ID
     *
     * @throws \framework\exceptions\InvalidObjectException When no objects are found by that ID
     * @throws \InvalidArgumentException When the parameter $id is not a int
     */
    public static function getByID($id) {
        if (is_numeric($id)) {
            $bean = \R::findOne(strtolower(get_real_class(get_called_class())), "id=?", array($id));
            // Nonexistant ID
            if (!$bean) {
                throw new \framework\exceptions\InvalidObjectException("Object by id '$id' does not exist");
            }

            return new Definition($bean);
        } else {
            throw new \InvalidArgumentException("Expected int got " . gettype($id));
        }
    }

}
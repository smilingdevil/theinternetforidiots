<?php
/**
 * Framework created by Evan Darwin.
 *
 * You may not redistribute this code without written permission from Evan Darwin.
 *
 * This work is licensed under a Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
 *
 * Information about this license is located here:
 * http://creativecommons.org/licenses/by-nc-nd/3.0/legalcode
 * or view the LICENSE file included for the full license.
 */

namespace framework\models;

class Definition extends Model {
    /**
     * @return string|null Returns the word
     */
    public function getWord() {
        return $this->bean->word;
    }

    /**
     * @return string|null Returns phonetic spelling
     */
    public function getPhonetic() {
        return $this->bean->phonetic;
    }

    /**
     * @return string|null Returns definition
     */
    public function getDefinition() {
        return $this->bean->definition;
    }

    /**
     * @return string|null Returns last modified time
     */
    public function getLastModified() {
        return $this->bean->last_edited;
    }
}
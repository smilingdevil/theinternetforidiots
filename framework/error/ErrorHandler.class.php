<?php
/**
 * Framework created by Evan Darwin.
 *
 * You may not redistribute this code without written permission from Evan Darwin.
 *
 * This work is licensed under a Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
 *
 * Information about this license is located here:
 * http://creativecommons.org/licenses/by-nc-nd/3.0/legalcode
 * or view the LICENSE file included for the full license.
 */

namespace framework\error;

class ErrorHandler {
    /**
     * Handles an exception, to be implemented later
     *
     * @param \Exception $e Exception to handle
     */
    public function handle(\Exception $e) {
        // To be used later
        self::FiveHundred($e);
    }

    /**
     * Throws a 403 error
     */
    public static function FourOhThree() {
        header("HTTP/1.1 403 Forbidden");
        \framework\view\View::render("error", ['error' => "403", 'error_msg' => "Access Denied"]);
    }

    /**
     * Throws a 404 error
     */
    public static function FourOhFour() {
        header("HTTP/1.1 404 Not Found");
        \framework\view\View::render("error", ['error' => "404", 'error_msg' => "File not Found"]);
    }

    /**
     * Throws a 500 error
     *
     * @param \Exception $e Optional Exception to display stacktrace
     */
    public static function FiveHundred(\Exception $e = NULL) {
        header("HTTP/1.1 500 Internal Server Error");
        \framework\view\View::render("error", ['error' => "500", 'error_msg' => "Internal Server Error"]);
    }

    /**
     * Throws a 503 error
     */
    public static function FiveOhThree() {
        header("HTTP/1.1 503 Service Unavailable");
        \framework\view\View::render("error", ['error' => "503", 'error_msg' => "Server Overloaded"]);
    }
}
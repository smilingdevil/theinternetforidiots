<?php
/**
 * Framework created by Evan Darwin.
 *
 * You may not redistribute this code without written permission from Evan Darwin.
 *
 * This work is licensed under a Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
 *
 * Information about this license is located here:
 * http://creativecommons.org/licenses/by-nc-nd/3.0/legalcode
 * or view the LICENSE file included for the full license.
 */

require 'functions.php';
init();
<?php
/**
 * Framework created by Evan Darwin.
 *
 * You may not redistribute this code without written permission from Evan Darwin.
 *
 * This work is licensed under a Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
 *
 * Information about this license is located here:
 * http://creativecommons.org/licenses/by-nc-nd/3.0/legalcode
 * or view the LICENSE file included for the full license.
 */

namespace framework\db;

define('DB_SEARCH_EXACT', 1);

class DBOBjectsList {
    private $list = array();

    public function __construct(array $values) {
        foreach ($values as $bean) {
            if (!$bean instanceof \RedBean_OODBBean) {
                throw new \InvalidArgumentException("Unknown type '" . gettype($bean) . "' is in Redbean array");

            }
            $vals = $bean->export();
            $id   = $vals['id'];
            unset($vals['id']);
            $this->list[$id] = (object)$vals;
        }
    }

    public function get($id) {
        return $this->list[$id];
    }

    public function size() {
        return sizeof($this->list);
    }

    public function getIterator() {
        return new \ArrayIterator($this->list);
    }

    public function find($key, $value, array $args = array()) {
        $exact = FALSE;
        foreach ($args as $arg => $val) {
            if ($arg == DB_SEARCH_EXACT) {
                if (!is_bool($val)) {
                    throw new \InvalidArgumentException("Expected boolean got " . gettype($val));
                } else {
                    $exact = $val;
                }
            }
        }

        $ret = array();

        foreach ($this->list as $id => $object) {
            if (!isset($object->{$key})) {
                throw new \framework\exceptions\InvalidObjectValueException("Expected there to be a value $key, but found none");
            } else {
                if ($exact) {
                    if ($object->$key === $value) {
                        $ret[] = \framework\models\Definition::getByID($id);
                    }
                } else {
                    if (is_string($object->$key)) {
                        if (strtolower($object->$key) === strtolower($value)) {
                            $ret[] = \framework\models\Definition::getByID($id);
                        }
                    } else {
                        if ($object->$key == $value) {
                            $ret[] = \framework\models\Definition::getByID($id);
                        }
                    }
                }
            }
        }

        return $ret;
    }
}
<?php
/**
 * Framework created by Evan Darwin.
 *
 * You may not redistribute this code without written permission from Evan Darwin.
 *
 * This work is licensed under a Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
 *
 * Information about this license is located here:
 * http://creativecommons.org/licenses/by-nc-nd/3.0/legalcode
 * or view the LICENSE file included for the full license.
 */

error_reporting(-1);

/**
 * Core subpackage
 */

require 'core/Globals.class.php';
require 'core/IntegerBoundaries.class.php';


/**
 * DB subpackage
 */

// RedBeanPHP
require 'db/rb.class.php';
require 'db/DBObjectsList.class.php';


/**
 * Error subpackage
 */

require 'error/ErrorHandler.class.php';


/**
 * External subpackage
 */

// WolframAlpha API
require 'external/WolframAlpha/WolframAlpha.class.php';
require 'external/WolframAlpha/WolframAlphaResult.class.php';


/**
 * Exceptions subpackage
 */

require 'exceptions/InvalidObjectException.class.php';
require 'exceptions/InvalidObjectValueException.class.php';


/**
 * Models subpackage
 */

require 'models/Model.class.php';
require 'models/Definition.class.php';

/**
 * Security subpackage
 */

require 'security/HeaderRewrite.class.php';


/**
 * View subpackage
 */

require 'view/View.class.php';


/**
 * Begin functions
 */
function connect_db() {
    // moved outside so git can't see
    require 'db.php';
}

function init_global_vars() {
    \framework\core\Globals::$ROOT     = dirname(__DIR__);
    \framework\core\Globals::$BASE_URL = 'http://theinternetforidiots.com';
}

function get_real_class($class) {
    $parts = explode("\\", $class);

    return $parts[sizeof($parts) - 1];
}

function make_path($dir = "") {
    return implode('/', array(\framework\core\Globals::$BASE_URL, $dir));
}

function static_url($url) {
    return make_path("static$url");
}

function makeDateTime($time = -1) {
    return ($time == -1) ? (date("Y-m-d H:i:s", time())) : (date("m/d/y g:i A", $time));
}

function redirect($url, $die = TRUE) {
    header("Location: $url");
    if ($die) {
        die;
    }
}

function fragment($file) {
    require implode(DIRECTORY_SEPARATOR, array(dirname(__DIR__), "fragments", "$file.php"));
}

function start_section() {
    ob_start();
}

function end_section($name) {
    $data = ob_get_clean();
    global $_SECTION;
    $_SECTION[$name] = $data;
}

function clear_section($name) {
    global $_SECTION;
    if (isset($_SECTION[$name])) {
        $_SECTION[$name] = "";
    }

}

function wolframPronunciationParse($result) {
    return explode(" ", $result)[0];
}

/**
 * !!! DON'T USE THIS IN MAIN THREAD, ONLY AJAX !!!
 *
 * @param $word string Word to lookup
 *
 * @return string|null Pronunciation
 */
function getPronunciation($word) {
    $bean = R::find("definition", "word LIKE ?", array($word));
    if (!$bean) {
        $wa     = new \framework\external\WolframAlpha\WolframAlphaAPI();
        $result = $wa->query($word);

        return wolframPronunciationParse($result->getByResultSet(WA_RESULT_TYPE_PRONUNCIATION)->getText());
    } else {
        return $bean->phonetic;
    }
}

function updatePageTitle($title) {
    \framework\view\View::$PAGE_TITLE = htmlentities($title);
}

function init() {
    date_default_timezone_set("America/Phoenix");
    connect_db();
    init_global_vars();

    # Force headers
    \framework\security\HeaderRewrite::secure();
}
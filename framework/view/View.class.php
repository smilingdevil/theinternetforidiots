<?php
/**
 * Framework created by Evan Darwin.
 *
 * You may not redistribute this code without written permission from Evan Darwin.
 *
 * This work is licensed under a Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
 *
 * Information about this license is located here:
 * http://creativecommons.org/licenses/by-nc-nd/3.0/legalcode
 * or view the LICENSE file included for the full license.
 */

namespace framework\view;

class View {
    public static $PAGE_TITLE = "";
    public static $PAGE = "";

    /**
     * @param       $page   Page to load
     * @param array $passed Values to pass to the page
     *
     * @throws Exception Invalid view
     */
    public static function render($page, array $passed = array()) {
        $url = implode(DIRECTORY_SEPARATOR, array(dirname(dirname(__DIR__)), "templates", $page . ".page.php"));
        if (@file_exists($url)) {
            global $_DATA;
            $_DATA = $passed;
            // Include base
            require $url;
        } else {
            // Die
            throw new \Exception("View not found: $url");
        }
    }
}